package iu

type Builder struct {
	containers Containers
}

func NewBuilder() *Builder {
	return &Builder{}
}

func (b *Builder) Add(containers ...Container) {
	for _, container := range containers {
		b.add(container)
	}
}

func (b *Builder) add(container Container) {
	b.containers[container.Name] = container
}

func (b *Builder) Get(name string) interface{} {
	return b.containers[name]
}
