package iu

type ContainerState interface {
	Get() interface{}
}

type Container struct {
	Name  string
	Build func(ctn ContainerState) (interface{}, error)
}
type Containers map[string]Container
