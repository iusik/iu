package iu

type App struct {
	Builder *Builder
}

func CreateApp() *App {
	return &App{
		Builder: NewBuilder(),
	}
}
